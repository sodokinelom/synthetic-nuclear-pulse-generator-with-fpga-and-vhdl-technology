------------------------------------------------------------------
-- Name		   : lfsr1.vhd
-- Description : Part of the LFSR tutorial
-- Designed by : Claudio Avi Chami - FPGA Site
--               https://fpgasite.wordpress.com
-- Date        : 09/Aug/2016
-- Version     : 01
------------------------------------------------------------------
library ieee;
    use ieee.std_logic_1164.all;

entity lfsr1 is
  port (
    n_reset  : in  std_logic;
    clk    : in  std_logic; 
    count  : out std_logic_vector (13 downto 0) -- lfsr output
  );
end entity;

architecture rtl of lfsr1 is
    signal count_i    	: std_logic_vector (13 downto 0);
    signal feedback 	: std_logic;

begin
    feedback <= not(count_i(13) xor count_i(12) xor count_i(11) xor count_i(1));		-- LFSR size 4

    process (n_reset, clk) 
	begin
        if (n_reset = '0') then
            count_i <= (others=>'0');
        elsif (rising_edge(clk)) then
			count_i <= count_i(12 downto 0) & feedback;
        end if;
    end process;
    count <= count_i;
end architecture;
