set project_name gen_pulse_200831
set part_name xc7z010clg400-1
set bd_path ./$project_name/$project_name.srcs/sources_1/bd/system

file delete -force ./$project_name
create_project $project_name ./$project_name -part $part_name

# define VHLD as default language
set_property  TARGET_LANGUAGE VHDL [current_project]

create_bd_design system

# Load RedPitaya ports
source cfg/ports.tcl

# Set Path for the custom IP cores
set_property  IP_REPO_PATHS  ./ip [current_project]
update_ip_catalog

# ====================================================================================
# Zynq processing system with RedPitaya specific preset
startgroup
# Create instance: processing_system7, and set properties
set processing_system7 [ create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7:5.5 processing_system7_0]
set_property -dict [ list \
	CONFIG.PCW_ACT_ENET0_PERIPHERAL_FREQMHZ {125.000000} \
	CONFIG.PCW_ACT_FPGA0_PERIPHERAL_FREQMHZ {125.000000} \
	CONFIG.PCW_ACT_FPGA1_PERIPHERAL_FREQMHZ {250.000000} \
	CONFIG.PCW_ACT_FPGA2_PERIPHERAL_FREQMHZ {50.000000} \
	CONFIG.PCW_ACT_FPGA3_PERIPHERAL_FREQMHZ {200.000000} \
	CONFIG.PCW_ACT_QSPI_PERIPHERAL_FREQMHZ {125.000000} \
	CONFIG.PCW_ACT_SDIO_PERIPHERAL_FREQMHZ {100.000000} \
	CONFIG.PCW_ACT_SPI_PERIPHERAL_FREQMHZ {200.000000} \
	CONFIG.PCW_ACT_UART_PERIPHERAL_FREQMHZ {100.000000} \
	CONFIG.PCW_CLK0_FREQ {125000000} \
	CONFIG.PCW_CLK1_FREQ {250000000} \
	CONFIG.PCW_CLK2_FREQ {50000000} \
	CONFIG.PCW_CLK3_FREQ {200000000} \
	CONFIG.PCW_ENET0_ENET0_IO {MIO 16 .. 27} \
	CONFIG.PCW_ENET0_GRP_MDIO_ENABLE {1} \
	CONFIG.PCW_ENET0_GRP_MDIO_IO {MIO 52 .. 53} \
	CONFIG.PCW_ENET0_PERIPHERAL_CLKSRC {IO PLL} \
	CONFIG.PCW_ENET0_PERIPHERAL_ENABLE {1} \
	CONFIG.PCW_ENET_RESET_ENABLE {1} \
	CONFIG.PCW_ENET_RESET_SELECT {Share reset pin} \
	CONFIG.PCW_EN_CLK1_PORT {0} \
	CONFIG.PCW_EN_CLK2_PORT {0} \
	CONFIG.PCW_EN_CLK3_PORT {0} \
	CONFIG.PCW_EN_EMIO_GPIO {1} \
	CONFIG.PCW_EN_EMIO_SPI0 {1} \
	CONFIG.PCW_EN_EMIO_TTC0 {1} \
	CONFIG.PCW_EN_ENET0 {1} \
	CONFIG.PCW_EN_GPIO {1} \
	CONFIG.PCW_EN_I2C0 {1} \
	CONFIG.PCW_EN_QSPI {1} \
	CONFIG.PCW_EN_RST1_PORT {0} \
	CONFIG.PCW_EN_RST2_PORT {0} \
	CONFIG.PCW_EN_RST3_PORT {0} \
	CONFIG.PCW_EN_SDIO0 {1} \
	CONFIG.PCW_EN_SPI0 {1} \
	CONFIG.PCW_EN_SPI1 {1} \
	CONFIG.PCW_EN_TTC0 {1} \
	CONFIG.PCW_EN_UART0 {1} \
	CONFIG.PCW_EN_UART1 {1} \
	CONFIG.PCW_EN_USB0 {1} \
	CONFIG.PCW_FCLK_CLK1_BUF {TRUE} \
	CONFIG.PCW_FCLK_CLK2_BUF {TRUE} \
	CONFIG.PCW_FCLK_CLK3_BUF {TRUE} \
	CONFIG.PCW_FPGA0_PERIPHERAL_FREQMHZ {125} \
	CONFIG.PCW_FPGA1_PERIPHERAL_FREQMHZ {250} \
	CONFIG.PCW_FPGA3_PERIPHERAL_FREQMHZ {200} \
	CONFIG.PCW_FPGA_FCLK1_ENABLE {1} \
	CONFIG.PCW_FPGA_FCLK2_ENABLE {1} \
	CONFIG.PCW_FPGA_FCLK3_ENABLE {1} \
	CONFIG.PCW_GPIO_EMIO_GPIO_ENABLE {1} \
	CONFIG.PCW_GPIO_EMIO_GPIO_IO {24} \
	CONFIG.PCW_GPIO_EMIO_GPIO_WIDTH {24} \
	CONFIG.PCW_GPIO_MIO_GPIO_ENABLE {1} \
	CONFIG.PCW_GPIO_MIO_GPIO_IO {MIO} \
	CONFIG.PCW_I2C0_I2C0_IO {MIO 50 .. 51} \
	CONFIG.PCW_I2C0_PERIPHERAL_ENABLE {1} \
	CONFIG.PCW_I2C_PERIPHERAL_FREQMHZ {111.111115} \
	CONFIG.PCW_I2C_RESET_ENABLE {1} \
	CONFIG.PCW_I2C_RESET_SELECT {Share reset pin} \
	CONFIG.PCW_IRQ_F2P_INTR {1} \
	CONFIG.PCW_MIO_0_IOTYPE {LVCMOS 3.3V} \
	CONFIG.PCW_MIO_0_PULLUP {enabled} \
	CONFIG.PCW_MIO_0_SLEW {slow} \
	CONFIG.PCW_MIO_10_IOTYPE {LVCMOS 3.3V} \
	CONFIG.PCW_MIO_10_PULLUP {enabled} \
	CONFIG.PCW_MIO_10_SLEW {slow} \
	CONFIG.PCW_MIO_11_IOTYPE {LVCMOS 3.3V} \
	CONFIG.PCW_MIO_11_PULLUP {enabled} \
	CONFIG.PCW_MIO_11_SLEW {slow} \
	CONFIG.PCW_MIO_12_IOTYPE {LVCMOS 3.3V} \
	CONFIG.PCW_MIO_12_PULLUP {enabled} \
	CONFIG.PCW_MIO_12_SLEW {slow} \
	CONFIG.PCW_MIO_13_IOTYPE {LVCMOS 3.3V} \
	CONFIG.PCW_MIO_13_PULLUP {enabled} \
	CONFIG.PCW_MIO_13_SLEW {slow} \
	CONFIG.PCW_MIO_14_IOTYPE {LVCMOS 3.3V} \
	CONFIG.PCW_MIO_14_PULLUP {enabled} \
	CONFIG.PCW_MIO_14_SLEW {slow} \
	CONFIG.PCW_MIO_15_IOTYPE {LVCMOS 3.3V} \
	CONFIG.PCW_MIO_15_PULLUP {enabled} \
	CONFIG.PCW_MIO_15_SLEW {slow} \
	CONFIG.PCW_MIO_16_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_16_PULLUP {disabled} \
	CONFIG.PCW_MIO_16_SLEW {fast} \
	CONFIG.PCW_MIO_17_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_17_PULLUP {disabled} \
	CONFIG.PCW_MIO_17_SLEW {fast} \
	CONFIG.PCW_MIO_18_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_18_PULLUP {disabled} \
	CONFIG.PCW_MIO_18_SLEW {fast} \
	CONFIG.PCW_MIO_19_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_19_PULLUP {disabled} \
	CONFIG.PCW_MIO_19_SLEW {fast} \
	CONFIG.PCW_MIO_1_IOTYPE {LVCMOS 3.3V} \
	CONFIG.PCW_MIO_1_PULLUP {enabled} \
	CONFIG.PCW_MIO_1_SLEW {slow} \
	CONFIG.PCW_MIO_20_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_20_PULLUP {disabled} \
	CONFIG.PCW_MIO_20_SLEW {fast} \
	CONFIG.PCW_MIO_21_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_21_PULLUP {disabled} \
	CONFIG.PCW_MIO_21_SLEW {fast} \
	CONFIG.PCW_MIO_22_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_22_PULLUP {disabled} \
	CONFIG.PCW_MIO_22_SLEW {fast} \
	CONFIG.PCW_MIO_23_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_23_PULLUP {disabled} \
	CONFIG.PCW_MIO_23_SLEW {fast} \
	CONFIG.PCW_MIO_24_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_24_PULLUP {disabled} \
	CONFIG.PCW_MIO_24_SLEW {fast} \
	CONFIG.PCW_MIO_25_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_25_PULLUP {disabled} \
	CONFIG.PCW_MIO_25_SLEW {fast} \
	CONFIG.PCW_MIO_26_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_26_PULLUP {disabled} \
	CONFIG.PCW_MIO_26_SLEW {fast} \
	CONFIG.PCW_MIO_27_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_27_PULLUP {disabled} \
	CONFIG.PCW_MIO_27_SLEW {fast} \
	CONFIG.PCW_MIO_28_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_28_PULLUP {disabled} \
	CONFIG.PCW_MIO_28_SLEW {fast} \
	CONFIG.PCW_MIO_29_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_29_PULLUP {disabled} \
	CONFIG.PCW_MIO_29_SLEW {fast} \
	CONFIG.PCW_MIO_2_IOTYPE {LVCMOS 3.3V} \
	CONFIG.PCW_MIO_2_SLEW {slow} \
	CONFIG.PCW_MIO_30_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_30_PULLUP {disabled} \
	CONFIG.PCW_MIO_30_SLEW {fast} \
	CONFIG.PCW_MIO_31_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_31_PULLUP {disabled} \
	CONFIG.PCW_MIO_31_SLEW {fast} \
	CONFIG.PCW_MIO_32_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_32_PULLUP {disabled} \
	CONFIG.PCW_MIO_32_SLEW {fast} \
	CONFIG.PCW_MIO_33_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_33_PULLUP {disabled} \
	CONFIG.PCW_MIO_33_SLEW {fast} \
	CONFIG.PCW_MIO_34_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_34_PULLUP {disabled} \
	CONFIG.PCW_MIO_34_SLEW {fast} \
	CONFIG.PCW_MIO_35_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_35_PULLUP {disabled} \
	CONFIG.PCW_MIO_35_SLEW {fast} \
	CONFIG.PCW_MIO_36_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_36_PULLUP {disabled} \
	CONFIG.PCW_MIO_36_SLEW {fast} \
	CONFIG.PCW_MIO_37_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_37_PULLUP {disabled} \
	CONFIG.PCW_MIO_37_SLEW {fast} \
	CONFIG.PCW_MIO_38_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_38_PULLUP {disabled} \
	CONFIG.PCW_MIO_38_SLEW {fast} \
	CONFIG.PCW_MIO_39_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_39_PULLUP {disabled} \
	CONFIG.PCW_MIO_39_SLEW {fast} \
	CONFIG.PCW_MIO_3_IOTYPE {LVCMOS 3.3V} \
	CONFIG.PCW_MIO_3_SLEW {slow} \
	CONFIG.PCW_MIO_40_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_40_PULLUP {enabled} \
	CONFIG.PCW_MIO_40_SLEW {slow} \
	CONFIG.PCW_MIO_41_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_41_PULLUP {enabled} \
	CONFIG.PCW_MIO_41_SLEW {slow} \
	CONFIG.PCW_MIO_42_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_42_PULLUP {enabled} \
	CONFIG.PCW_MIO_42_SLEW {slow} \
	CONFIG.PCW_MIO_43_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_43_PULLUP {enabled} \
	CONFIG.PCW_MIO_43_SLEW {slow} \
	CONFIG.PCW_MIO_44_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_44_PULLUP {enabled} \
	CONFIG.PCW_MIO_44_SLEW {slow} \
	CONFIG.PCW_MIO_45_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_45_PULLUP {enabled} \
	CONFIG.PCW_MIO_45_SLEW {slow} \
	CONFIG.PCW_MIO_46_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_46_PULLUP {enabled} \
	CONFIG.PCW_MIO_46_SLEW {slow} \
	CONFIG.PCW_MIO_47_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_47_PULLUP {enabled} \
	CONFIG.PCW_MIO_47_SLEW {slow} \
	CONFIG.PCW_MIO_48_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_48_PULLUP {enabled} \
	CONFIG.PCW_MIO_48_SLEW {slow} \
	CONFIG.PCW_MIO_49_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_49_PULLUP {enabled} \
	CONFIG.PCW_MIO_49_SLEW {slow} \
	CONFIG.PCW_MIO_4_IOTYPE {LVCMOS 3.3V} \
	CONFIG.PCW_MIO_4_SLEW {slow} \
	CONFIG.PCW_MIO_50_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_50_PULLUP {enabled} \
	CONFIG.PCW_MIO_50_SLEW {slow} \
	CONFIG.PCW_MIO_51_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_51_PULLUP {enabled} \
	CONFIG.PCW_MIO_51_SLEW {slow} \
	CONFIG.PCW_MIO_52_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_52_PULLUP {enabled} \
	CONFIG.PCW_MIO_52_SLEW {slow} \
	CONFIG.PCW_MIO_53_IOTYPE {LVCMOS 2.5V} \
	CONFIG.PCW_MIO_53_PULLUP {enabled} \
	CONFIG.PCW_MIO_53_SLEW {slow} \
	CONFIG.PCW_MIO_5_IOTYPE {LVCMOS 3.3V} \
	CONFIG.PCW_MIO_5_SLEW {slow} \
	CONFIG.PCW_MIO_6_IOTYPE {LVCMOS 3.3V} \
	CONFIG.PCW_MIO_6_SLEW {slow} \
	CONFIG.PCW_MIO_7_IOTYPE {LVCMOS 3.3V} \
	CONFIG.PCW_MIO_7_SLEW {slow} \
	CONFIG.PCW_MIO_8_IOTYPE {LVCMOS 3.3V} \
	CONFIG.PCW_MIO_8_SLEW {slow} \
	CONFIG.PCW_MIO_9_IOTYPE {LVCMOS 3.3V} \
	CONFIG.PCW_MIO_9_PULLUP {enabled} \
	CONFIG.PCW_MIO_9_SLEW {slow} \
	CONFIG.PCW_MIO_TREE_PERIPHERALS {GPIO#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#GPIO#UART 1#UART 1#SPI 1#SPI 1#SPI 1#SPI 1#UART 0#UART 0#Enet 0#Enet 0#Enet 0#Enet 0#Enet 0#Enet 0#Enet 0#Enet 0#Enet 0#Enet 0#Enet 0#Enet 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#SD 0#SD 0#SD 0#SD 0#SD 0#SD 0#SD 0#SD 0#USB Reset#GPIO#I2C 0#I2C 0#Enet 0#Enet 0} \
	CONFIG.PCW_MIO_TREE_SIGNALS {gpio[0]#qspi0_ss_b#qspi0_io[0]#qspi0_io[1]#qspi0_io[2]#qspi0_io[3]#qspi0_sclk#gpio[7]#tx#rx#mosi#miso#sclk#ss[0]#rx#tx#tx_clk#txd[0]#txd[1]#txd[2]#txd[3]#tx_ctl#rx_clk#rxd[0]#rxd[1]#rxd[2]#rxd[3]#rx_ctl#data[4]#dir#stp#nxt#data[0]#data[1]#data[2]#data[3]#clk#data[5]#data[6]#data[7]#clk#cmd#data[0]#data[1]#data[2]#data[3]#cd#wp#reset#gpio[49]#scl#sda#mdc#mdio} \
	CONFIG.PCW_PACKAGE_DDR_BOARD_DELAY0 {0.080} \
	CONFIG.PCW_PACKAGE_DDR_BOARD_DELAY1 {0.063} \
	CONFIG.PCW_PACKAGE_DDR_BOARD_DELAY2 {0.057} \
	CONFIG.PCW_PACKAGE_DDR_BOARD_DELAY3 {0.068} \
	CONFIG.PCW_PACKAGE_DDR_DQS_TO_CLK_DELAY_0 {-0.047} \
	CONFIG.PCW_PACKAGE_DDR_DQS_TO_CLK_DELAY_1 {-0.025} \
	CONFIG.PCW_PACKAGE_DDR_DQS_TO_CLK_DELAY_2 {-0.006} \
	CONFIG.PCW_PACKAGE_DDR_DQS_TO_CLK_DELAY_3 {-0.017} \
	CONFIG.PCW_PRESET_BANK1_VOLTAGE {LVCMOS 2.5V} \
	CONFIG.PCW_QSPI_GRP_SINGLE_SS_ENABLE {1} \
	CONFIG.PCW_QSPI_GRP_SINGLE_SS_IO {MIO 1 .. 6} \
	CONFIG.PCW_QSPI_PERIPHERAL_CLKSRC {IO PLL} \
	CONFIG.PCW_QSPI_PERIPHERAL_ENABLE {1} \
	CONFIG.PCW_QSPI_PERIPHERAL_FREQMHZ {125} \
	CONFIG.PCW_QSPI_QSPI_IO {MIO 1 .. 6} \
	CONFIG.PCW_SD0_GRP_CD_ENABLE {1} \
	CONFIG.PCW_SD0_GRP_CD_IO {MIO 46} \
	CONFIG.PCW_SD0_GRP_WP_ENABLE {1} \
	CONFIG.PCW_SD0_GRP_WP_IO {MIO 47} \
	CONFIG.PCW_SD0_PERIPHERAL_ENABLE {1} \
	CONFIG.PCW_SD0_SD0_IO {MIO 40 .. 45} \
	CONFIG.PCW_SDIO_PERIPHERAL_VALID {1} \
	CONFIG.PCW_SPI0_PERIPHERAL_ENABLE {1} \
	CONFIG.PCW_SPI0_SPI0_IO {EMIO} \
	CONFIG.PCW_SPI1_PERIPHERAL_ENABLE {1} \
	CONFIG.PCW_SPI1_SPI1_IO {MIO 10 .. 15} \
	CONFIG.PCW_SPI_PERIPHERAL_FREQMHZ {200} \
	CONFIG.PCW_SPI_PERIPHERAL_VALID {1} \
	CONFIG.PCW_TTC0_PERIPHERAL_ENABLE {1} \
	CONFIG.PCW_TTC0_TTC0_IO {EMIO} \
	CONFIG.PCW_UART0_PERIPHERAL_ENABLE {1} \
	CONFIG.PCW_UART0_UART0_IO {MIO 14 .. 15} \
	CONFIG.PCW_UART1_PERIPHERAL_ENABLE {1} \
	CONFIG.PCW_UART1_UART1_IO {MIO 8 .. 9} \
	CONFIG.PCW_UART_PERIPHERAL_VALID {1} \
	CONFIG.PCW_UIPARAM_DDR_BUS_WIDTH {16 Bit} \
	CONFIG.PCW_UIPARAM_DDR_CLOCK_0_PACKAGE_LENGTH {54.563} \
	CONFIG.PCW_UIPARAM_DDR_CLOCK_1_PACKAGE_LENGTH {54.563} \
	CONFIG.PCW_UIPARAM_DDR_CLOCK_2_PACKAGE_LENGTH {54.563} \
	CONFIG.PCW_UIPARAM_DDR_CLOCK_3_PACKAGE_LENGTH {54.563} \
	CONFIG.PCW_UIPARAM_DDR_DQS_0_PACKAGE_LENGTH {101.239} \
	CONFIG.PCW_UIPARAM_DDR_DQS_1_PACKAGE_LENGTH {79.5025} \
	CONFIG.PCW_UIPARAM_DDR_DQS_2_PACKAGE_LENGTH {60.536} \
	CONFIG.PCW_UIPARAM_DDR_DQS_3_PACKAGE_LENGTH {71.7715} \
	CONFIG.PCW_UIPARAM_DDR_DQ_0_PACKAGE_LENGTH {104.5365} \
	CONFIG.PCW_UIPARAM_DDR_DQ_1_PACKAGE_LENGTH {70.676} \
	CONFIG.PCW_UIPARAM_DDR_DQ_2_PACKAGE_LENGTH {59.1615} \
	CONFIG.PCW_UIPARAM_DDR_DQ_3_PACKAGE_LENGTH {81.319} \
	CONFIG.PCW_UIPARAM_DDR_PARTNO {MT41J256M16 RE-125} \
	CONFIG.PCW_USB0_PERIPHERAL_ENABLE {1} \
	CONFIG.PCW_USB0_RESET_ENABLE {1} \
	CONFIG.PCW_USB0_RESET_IO {MIO 48} \
	CONFIG.PCW_USB0_USB0_IO {MIO 28 .. 39} \
	CONFIG.PCW_USB_RESET_ENABLE {1} \
	CONFIG.PCW_USB_RESET_SELECT {Share reset pin} \
	CONFIG.PCW_USE_FABRIC_INTERRUPT {1} \
	CONFIG.PCW_USE_M_AXI_GP1 {0} \
	CONFIG.PCW_USE_S_AXI_HP0 {1} \
	CONFIG.PCW_USE_S_AXI_HP1 {0} \
 ] $processing_system7
 endgroup

# Buffers for differential IOs
startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf util_ds_buf_0
set_property -dict [list CONFIG.C_SIZE {1}] [get_bd_cells util_ds_buf_0]

create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf util_ds_buf_1
set_property -dict [list CONFIG.C_SIZE {2}] [get_bd_cells util_ds_buf_1]

create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf util_ds_buf_2
set_property -dict [list CONFIG.C_SIZE {2}] [get_bd_cells util_ds_buf_2]
set_property -dict [list CONFIG.C_BUF_TYPE {OBUFDS}] [get_bd_cells util_ds_buf_2]
endgroup


# Timer variable
startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:axi_timer:2.0 axi_timer_0
set_property -dict [list CONFIG.mode_64bit {0}] [get_bd_cells axi_timer_0]
endgroup


# Impulsion
startgroup
create_bd_cell -type ip -vlnv cea.fr:user:impulsion:1.0 impulsion_0
endgroup


# Générateur de bruit Lfsr
startgroup
create_bd_cell -type ip -vlnv cea.fr:user:lfsr1:1.0 lfsr1_0
endgroup

# Générateur de start (Timer)
startgroup
create_bd_cell -type ip -vlnv cea.fr:user:clk100KHz:1.0 clk100KHz_0
endgroup

# Additionneur
startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:c_addsub:12.0 c_addsub_0
endgroup

# Configuration des ports de l'additionneur en 14 bits
set_property -dict [list CONFIG.A_Width.VALUE_SRC USER CONFIG.B_Width.VALUE_SRC USER] [get_bd_cells c_addsub_0]
set_property -dict [list CONFIG.A_Width {14} CONFIG.B_Width {14} CONFIG.Out_Width {14} CONFIG.Latency {1} CONFIG.B_Value {00000000000000}] [get_bd_cells c_addsub_0]


# Génération du FCLK1 à 250 MHz
startgroup
set_property -dict [list CONFIG.PCW_EN_CLK1_PORT {1}] [get_bd_cells processing_system7_0]
endgroup

# Constant avec valeur 1
startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_0
endgroup

# Constant avec valeur 0
startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_1
endgroup
set_property -dict [list CONFIG.CONST_VAL {0}] [get_bd_cells xlconstant_1]


# ====================================================================================
# Connections

# AXI autoconnect
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {Auto} Clk_slave {Auto} Clk_xbar {Auto} Master {/processing_system7_0/M_AXI_GP0} Slave {/axi_timer_0/S_AXI} ddr_seg {Auto} intc_ip {New AXI Interconnect} master_apm {0}}  [get_bd_intf_pins axi_timer_0/S_AXI]


# Buffers
connect_bd_net [get_bd_ports adc_clk_p_i] [get_bd_pins util_ds_buf_0/IBUF_DS_P]
connect_bd_net [get_bd_ports adc_clk_n_i] [get_bd_pins util_ds_buf_0/IBUF_DS_N]
connect_bd_net [get_bd_ports daisy_p_i] [get_bd_pins util_ds_buf_1/IBUF_DS_P]
connect_bd_net [get_bd_ports daisy_n_i] [get_bd_pins util_ds_buf_1/IBUF_DS_N]
connect_bd_net [get_bd_ports daisy_p_o] [get_bd_pins util_ds_buf_2/OBUF_DS_P]
connect_bd_net [get_bd_ports daisy_n_o] [get_bd_pins util_ds_buf_2/OBUF_DS_N]
connect_bd_net [get_bd_pins util_ds_buf_1/IBUF_OUT] [get_bd_pins util_ds_buf_2/OBUF_IN]
apply_bd_automation -rule xilinx.com:bd_rule:processing_system7 -config {make_external "FIXED_IO, DDR" Master "Disable" Slave "Disable" }  [get_bd_cells processing_system7_0]

# Timer variable
connect_bd_net [get_bd_pins axi_timer_0/capturetrig0] [get_bd_pins processing_system7_0/FCLK_CLK0]
connect_bd_net [get_bd_pins axi_timer_0/capturetrig1] [get_bd_pins processing_system7_0/FCLK_CLK0] 


# Zynq
connect_bd_net [get_bd_pins processing_system7_0/S_AXI_HP0_ACLK] [get_bd_pins processing_system7_0/FCLK_CLK0]


# FCLK_CLK0 Connections
connect_bd_net [get_bd_pins impulsion_0/BRAM_PORTA_0_dout] [get_bd_pins c_addsub_0/A]
connect_bd_net [get_bd_pins lfsr1_0/count] [get_bd_pins c_addsub_0/B]
connect_bd_net [get_bd_pins clk100KHz_0/clk_out] [get_bd_pins impulsion_0/start]
connect_bd_net [get_bd_pins lfsr1_0/clk] [get_bd_pins processing_system7_0/FCLK_CLK0]
connect_bd_net [get_bd_pins c_addsub_0/CLK] [get_bd_pins processing_system7_0/FCLK_CLK0]
connect_bd_net [get_bd_pins impulsion_0/clk] [get_bd_pins processing_system7_0/FCLK_CLK0]
connect_bd_net [get_bd_pins clk100KHz_0/clk_in] [get_bd_pins processing_system7_0/FCLK_CLK0]


#reset connnection
connect_bd_net [get_bd_pins lfsr1_0/n_reset] [get_bd_pins clk100KHz_0/n_reset]
connect_bd_net [get_bd_pins impulsion_0/n_reset] [get_bd_pins lfsr1_0/n_reset]
connect_bd_net [get_bd_pins rst_ps7_0_125M/peripheral_aresetn] [get_bd_pins lfsr1_0/n_reset]


# Connection with clock enable aff the adder
connect_bd_net [get_bd_pins xlconstant_0/dout] [get_bd_pins c_addsub_0/CE]

# Connectionavec le dac
connect_bd_net [get_bd_ports dac_clk_o] [get_bd_pins processing_system7_0/FCLK_CLK1]
connect_bd_net [get_bd_ports dac_dat_o] [get_bd_pins c_addsub_0/S]
connect_bd_net [get_bd_ports dac_wrt_o] [get_bd_pins processing_system7_0/FCLK_CLK0]
connect_bd_net [get_bd_ports dac_sel_o] [get_bd_pins xlconstant_1/dout]
connect_bd_net [get_bd_ports dac_rst_o] [get_bd_pins xlconstant_1/dout]






























# ====================================================================================
# beautify layout
regenerate_bd_layout

# ====================================================================================
# Generate output products and wrapper, add constraint any any additional files

# Enable XPM FIFO
set_property XPM_LIBRARIES XPM_FIFO [current_project]

generate_target all [get_files  $bd_path/system.bd]

make_wrapper -files [get_files $bd_path/system.bd] -top
add_files -norecurse $bd_path/hdl/system_wrapper.vhd

# Load RedPitaya constraint files
set files [glob -nocomplain cfg/*.xdc]
if {[llength $files] > 0} {
  add_files -norecurse -fileset constrs_1 $files
}

# ====================================================================================
# Vivado synthesis and P&R strategies

set_property STRATEGY Flow_PerfOptimized_High [get_runs synth_1]
set_property STRATEGY Performance_NetDelay_high [get_runs impl_1]
